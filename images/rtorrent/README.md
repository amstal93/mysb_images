# rTorrent / ruTorrent

## Docker environment

## config.d - Variabled parameters

### NETWORK_BIND_ADDRESS_SET _(network.bind_address.set)_

Bind listening socket and outgoing connections to this network interface address.

```bash
network.bind_address.set = <local_address>
```

### NETWORK_LOCAL_ADDRESS_SET _(network.local_address.set)_

Set the address reported to the tracker.

```bash
network.local_address.set = <local_address>
```

### NETWORK_SCGI_OPEN_PORT _(network.scgi.open_port)_

Try to open a listening port in the range a up to and including b.

```bash
network.scgi.open_port = 127.0.0.1:<scgi_port>
```

### PIECES_MEMORY_MAX_SET _(pieces.memory.max.set)_

```bash
pieces.memory.max.set = 3500M
```
