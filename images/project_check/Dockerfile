#  __/\\\\____________/\\\\___________________/\\\\\\\\\\\____/\\\\\\\\\\\\\___
#   _\/\\\\\\________/\\\\\\_________________/\\\/////////\\\_\/\\\/////////\\\_
#	_\/\\\//\\\____/\\\//\\\____/\\\__/\\\__\//\\\______\///__\/\\\_______\/\\\_
#	 _\/\\\\///\\\/\\\/_\/\\\___\//\\\/\\\____\////\\\_________\/\\\\\\\\\\\\\\__
#	  _\/\\\__\///\\\/___\/\\\____\//\\\\\________\////\\\______\/\\\/////////\\\_
#	   _\/\\\____\///_____\/\\\_____\//\\\____________\////\\\___\/\\\_______\/\\\_
#		_\/\\\_____________\/\\\__/\\_/\\\______/\\\______\//\\\__\/\\\_______\/\\\_
#		 _\/\\\_____________\/\\\_\//\\\\/______\///\\\\\\\\\\\/___\/\\\\\\\\\\\\\/__
#		  _\///______________\///___\////__________\///////////_____\/////////////_____
#			By toulousain79 ---> https://github.com/toulousain79/
#
######################################################################
#
#	Copyright (c) 2013 toulousain79 (https://github.com/toulousain79/)
#	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#	--> Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
#
######################################################################
#### Arguments (outside of a build stage)
ARG REGISTRY_IMAGE

##### Base image
FROM ${REGISTRY_IMAGE}/base-debian/base-debian:latest

#### Arguments (inside of a build stage)
ARG PROJECT_NAMESPACE
ARG PROJECT_NAME
ARG BUILD_DATE
ARG BUILD_VERSION
ARG BUILD_REF
ARG PROJECT_NAME
ARG PROJECT_DESCRIPTION
ARG PROJECT_URL

#### Environment variables
# Use in multi-phase builds, when an init process requests for the container to gracefully exit, so that it may be committed
# Used with alternative CMD (worker.sh), leverages supervisor to maintain long-running processes
ENV PROJECT_NAMESPACE=${PROJECT_NAMESPACE} \
	PROJECT_NAME=${PROJECT_NAME} \
	BUILD_DATE=${BUILD_DATE} \
	BUILD_VERSION=${BUILD_VERSION} \
	BUILD_REF=${BUILD_REF} \
	PROJECT_DESCRIPTION=${PROJECT_DESCRIPTION} \
	PROJECT_URL=${PROJECT_URL} \
	S6_KILL_GRACETIME=3000 \
	S6_KILL_FINISH_MAXTIME=5000 \
	S6_BEHAVIOUR_IF_STAGE2_FAILS=2 \
	SIGNAL_BUILD_STOP=99 \
	DEBIAN_FRONTEND="noninteractive"

#### Metadata
LABEL maintainer="${PROJECT_NAMESPACE}" \
	org.label-schema.build-date="${BUILD_DATE}" \
	org.label-schema.build-version="${BUILD_VERSION}" \
	org.label-schema.build-ref="${BUILD_REF}" \
	org.label-schema.name="${PROJECT_NAME} - Project Check" \
	org.label-schema.description="${PROJECT_DESCRIPTION}" \
	org.label-schema.url="${PROJECT_URL}" \
	org.label-schema.vendor="MySB - ${PROJECT_NAMESPACE}" \
	org.label-schema.usage="${PROJECT_URL}/blob/${BUILD_VERSION}/README.md" \
	org.label-schema.changelog-url="${PROJECT_URL}/blob/${BUILD_VERSION}/Changelog.md"

#### Copy files
COPY ./rootfs /tmp/rootfs

#### First
RUN \
	cp -rv /tmp/rootfs/* / \
	&& chmod +x /usr/local/bin/* \
	&& /bin/ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime \
	&& /bin/ln -fs /usr/share/zoneinfo/${TZ} /etc/timezone \
	&& docker_clean

#### Debian Packages & Python packages
# PKG_BASE: Packages needed for your image to add to 'rootfs/install/pkg_base.txt'.
# PKG_TEMP: Packages needed only for the build and can be remove to add to 'rootfs/install/pkg_temp.txt'.
# PKG_TO_DEL: Package alraedy installed but not needed and to remove to add to 'rootfs/install/pkg_to_del.txt'.
# PIP_BASE: Python 2 packages to add to 'rootfs/install/requirements2.txt'.
# PIP3_BASE: Python 3 packages to add to 'rootfs/install/requirements3.txt'.
# NOTE: You must add python-pip or python3-pip into PKG_TEMP if you want use PIP or PIP3 !!!
# NOTE: Leave following with one layer !!!
RUN \
	# Debian packages
	aptinstaller \
	# Python
	&& pipinstaller \
	# Cleaning
	&& docker_clean

#### Shellcheck
RUN curl --retry 3 -k -L -S -o /tmp/shellcheck-stable.linux.x86_64.tar.xz https://storage.googleapis.com/shellcheck/shellcheck-stable.linux.x86_64.tar.xz &&\
	tar --xz -xvf /tmp/shellcheck-stable.linux.x86_64.tar.xz -C /tmp/ &&\
	cp /tmp/shellcheck-stable/shellcheck /usr/local/bin/shellcheck &&\
	chmod -v +x /usr/local/bin/shellcheck \
	&& docker_clean

#### Cleaning
RUN docker_clean 'END'

#### Goss validation
RUN /usr/local/bin/goss -g /goss.base.yaml validate
